# Apache-Airflow-Example

- Graphs: They are nodes with dependencies, comes from family tree, they have nodes and vertices. (Tree, Cycle, Directed).
- DAG: Each workflow in can be represented as a DAG, its directed (think on way trafic), acyclic (think as a tree like structure), no loop, its like  a state machine.


## DAG's in Airflow
- Each nodes can be represented as a task
- Each edges is a dependency between the task

# Install Enviroment

## VirtualEnv

```bash
VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
sudo pacman -S  python-virtualenv
sudo pacman -S community/python-virtualenvwrapper
```

Agregar en ~/.bashrc 

```bash
export WORKON_HOME=~/.virtualenvs
source /usr/bin/virtualenvwrapper.sh
```

ejecutar
```bash
source ~/.bashrc
mkvirtualenv -p /usr/bin/python3 venv
workon venv
```

## Pip install (No es necesario)

```bash
sudo pacman -S extra/python-pip
```


## Install Airflow
- Entr to the virtualenv
-  Ejecute:

```bash
pip install  apache-airflow
pip install --upgrade Flask
```

- apache airflow extra pkgs

```bash
pip install apache-airflow[postgres,s3]
pip install -U apache-airflow[celery,s3,postgres,jdbc,gcp_api,crypto,kubernetes]
```


- Crear una db para airflow

```bash
airflow initdb
```

## Airflow webservice

airflow webserver


## Exameple de Apache Airflow

### E1 
Hola mundo
```bash
python e1.py
```

```bash
airflow list_dags
```


- https://wiki.archlinux.org/index.php/Python/Virtual_environment_(Espa%C3%B1ol)
- https://blog.usejournal.com/testing-in-airflow-part-1-dag-validation-tests-dag-definition-tests-and-unit-tests-2aa94970570c
- http://michal.karzynski.pl/blog/2017/03/19/developing-workflows-with-apache-airflow/ (Probar esto)
