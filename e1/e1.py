from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
#slack
from airflow.hooks.base_hook import BaseHook
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator

from datetime import datetime, timedelta
from airflow.models import Variable

#XCOM abbreviation for cross communication
#push and pull


default_args = {
    'owner': 'leo',
    'depends_on_past': False,
    'start_date': datetime(2019, 8, 1),
    'email': ['leonardo.bravo@mail.udp.cl'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=2),
}

dag = DAG( 'HelloDag', default_args=default_args, schedule_interval="0 * * * *")
#dag = DAG( 'HelloDag', default_args=default_args, schedule_interval=timedelta(days=1))

Stage1 = BashOperator(
    task_id='Hello',
    bash_command='echo hello',
    dag=dag)

Stage2 = BashOperator(
    task_id='World',
    bash_command='echo world',
    dag=dag)

Stage1 >> Stage2
